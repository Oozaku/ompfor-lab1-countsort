#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

/* count sort parallel */
double count_sort_parallel(double a[], int n) {
	int i, j;
	double *temp;
	double start, end, duracao;

	temp = (double *)malloc(n*sizeof(double));

	start = omp_get_wtime();
#   pragma omp parallel for

    for (i = 0; i < n; i++) {
		int count = 0;
		for (j = 0; j < n; j++)
			if (a[j] < a[i])
				count+=1;
			else if (a[j] == a[i] && j < i)
				count+=1;
		temp[count] = a[i];
	}
	end = omp_get_wtime();

	duracao = end - start;

	memcpy(a, temp, n*sizeof(double));
	free(temp);

	return duracao;
}

/* count sort serial */
double count_sort_serial(double a[], int n) {
	int i, j, count;
	double *temp;
	double start, end, duracao;

	temp = (double *)malloc(n*sizeof(double));

	start = omp_get_wtime();
	for (i = 0; i < n; i++) {
		count = 0;
		for (j = 0; j < n; j++)
			if (a[j] < a[i])
				count++;
			else if (a[j] == a[i] && j < i)
				count++;
		temp[count] = a[i];
	}
	end = omp_get_wtime();

	duracao = end - start;

	memcpy(a, temp, n*sizeof(double));
	free(temp);

	return duracao;
}

int main(int argc, char * argv[]) {
	int i, n, nt;
	double  * a, t_s;
    double *b, t_p;
	scanf("%d",&nt);
	
	/* numero de valores */
	scanf("%d",&n);

	/* aloca os vetores de valores para o teste em serial(b) e para o teste em paralelo(a) */
	a = (double *)malloc(n*sizeof(double));
    b = (double *)malloc(n*sizeof(double));
	/* entrada dos valores */
	for(i=0;i<n;i++)
		scanf("%lf",&a[i]);
    memcpy(b, a, n*sizeof(double));

	/* chama as funcoes de count sort em paralelo e em serial */
	t_p = count_sort_parallel(a,n);
	t_s = count_sort_serial(b,n);
    
    /* compara lista a, b para ver se o programa em paralelo esta correto */
    int correct = 1;
    for (i=0;i<n;i++){
            if (a[i] != b[i]){
                correct=0;
                printf("Line %i is not correct:\na: %lf\nb: %lf\n",i,a[i],b[i]);
            }
    }

	/* Imprime o vetor ordenado */
	/*
    for(i=0;i<n;i++)
		printf("%.2lf ",a[i]);

	printf("\n");
    */
	/* imprime os tempos obtidos e o speedup */
	printf("Tempo serie: %lf\n",t_s);
    printf("Tempo paralelo: %lf\n",t_p);
    printf("Speedup: %lf\n",t_s/t_p);

	return 0;
}
